
ifneq ($(strip $(TARGET_BOARD_PLATFORM)), sofia3gr)
BOARD_CONNECTIVITY_VENDOR := Broadcom
BOARD_CONNECTIVITY_MODULE := ap6xxx
endif

ifeq ($(strip $(BOARD_CONNECTIVITY_VENDOR)), Broadcom)
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
WPA_SUPPLICANT_VERSION      := VER_0_8_X
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_bcmdhd
BOARD_HOSTAPD_DRIVER        := NL80211
BOARD_HOSTAPD_PRIVATE_LIB   := lib_driver_cmd_bcmdhd
BOARD_WLAN_DEVICE           := bcmdhd
WIFI_DRIVER_FW_PATH_PARAM   := "/sys/module/bcmdhd/parameters/firmware_path"
WIFI_DRIVER_FW_PATH_STA     := "/vendor/etc/firmware/fw_bcm4329.bin"
WIFI_DRIVER_FW_PATH_P2P     := "/vendor/etc/firmware/fw_bcm4329_p2p.bin"
WIFI_DRIVER_FW_PATH_AP      := "/vendor/etc/firmware/fw_bcm4329_apsta.bin"
endif

# bluetooth support
ifeq ($(strip $(BOARD_CONNECTIVITY_VENDOR)), Broadcom)
BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_BCM := true
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR ?= device/rockchip/$(TARGET_BOARD_PLATFORM)/bluetooth

ifeq ($(strip $(PRODUCT_BUILD_MODULE)), px5car)
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR ?= device/rockchip/px5/bluetooth
endif

ifeq ($(strip $(PRODUCT_BUILD_MODULE)), px3car)
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR ?= device/rockchip/px3/bluetooth
endif

ifeq ($(strip $(BOARD_CONNECTIVITY_MODULE)), ap6xxx_gps)
BLUETOOTH_USE_BPLUS := true
BLUETOOTH_ENABLE_FM := false
endif
endif

# Infineon Wi-Fi support
ifeq ($(strip $(BOARD_CONNECTIVITY_VENDOR)), Infineon)
BOARD_WPA_SUPPLICANT_DRIVER := NL80211
WPA_SUPPLICANT_VERSION      := VER_0_8_X
BOARD_WPA_SUPPLICANT_PRIVATE_LIB := lib_driver_cmd_ifx
BOARD_HOSTAPD_DRIVER        := NL80211
BOARD_HOSTAPD_PRIVATE_LIB   := lib_driver_cmd_ifx
BOARD_WLAN_DEVICE           := infineon
WIFI_DRIVER_FW_PATH_PARAM   := "/sys/module/bcmdhd/parameters/firmware_path"

ifeq ($(strip $(BOARD_CONNECTIVITY_MODULE)), cyw43438)
WIFI_DRIVER_FW_PATH_STA     := "/vendor/etc/firmware/cyw43438.bin"
WIFI_DRIVER_FW_PATH_P2P     := "/vendor/etc/firmware/cyw43438.bin"
WIFI_DRIVER_FW_PATH_AP      := "/vendor/etc/firmware/cyw43438.bin"
endif

ifeq ($(strip $(BOARD_CONNECTIVITY_MODULE)), cyw43455)
WIFI_DRIVER_FW_PATH_STA     := "/vendor/etc/firmware/cyw43455.bin"
WIFI_DRIVER_FW_PATH_P2P     := "/vendor/etc/firmware/cyw43455.bin"
WIFI_DRIVER_FW_PATH_AP      := "/vendor/etc/firmware/cyw43455.bin"
endif

ifeq ($(strip $(BOARD_CONNECTIVITY_MODULE)), cyw4354)
WIFI_DRIVER_FW_PATH_STA     := "/vendor/etc/firmware/cyw4354.bin"
WIFI_DRIVER_FW_PATH_P2P     := "/vendor/etc/firmware/cyw4354.bin"
WIFI_DRIVER_FW_PATH_AP      := "/vendor/etc/firmware/cyw4354.bin"
endif

ifeq ($(strip $(BOARD_CONNECTIVITY_MODULE)), cyw4373)
WIFI_DRIVER_FW_PATH_STA     := "/vendor/etc/firmware/cyw4373.bin"
WIFI_DRIVER_FW_PATH_P2P     := "/vendor/etc/firmware/cyw4373.bin"
WIFI_DRIVER_FW_PATH_AP      := "/vendor/etc/firmware/cyw4373.bin"
endif

ifeq ($(strip $(BOARD_CONNECTIVITY_MODULE)), cyw54591)
WIFI_DRIVER_FW_PATH_STA     := "/vendor/etc/firmware/cyw54591.bin"
WIFI_DRIVER_FW_PATH_P2P     := "/vendor/etc/firmware/cyw54591.bin"
WIFI_DRIVER_FW_PATH_AP      := "/vendor/etc/firmware/cyw54591.bin"
endif

ifeq ($(strip $(BOARD_CONNECTIVITY_MODULE)), cyw5557x)
WIFI_DRIVER_FW_PATH_STA     := "/vendor/etc/firmware/cyw5557x.bin"
WIFI_DRIVER_FW_PATH_P2P     := "/vendor/etc/firmware/cyw5557x.bin"
WIFI_DRIVER_FW_PATH_AP      := "/vendor/etc/firmware/cyw5557x.bin"
endif
endif

# Infineon Bluetooth support
ifeq ($(strip $(BOARD_CONNECTIVITY_VENDOR)), Infineon)
BOARD_HAVE_BLUETOOTH := true
BOARD_HAVE_BLUETOOTH_IFX := true
BOARD_BLUETOOTH_BDROID_BUILDCFG_INCLUDE_DIR ?= device/rockchip/$(TARGET_BOARD_PLATFORM)/bluetooth
endif

BOARD_HAVE_BLUETOOTH_RTK := true

